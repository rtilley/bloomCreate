# bloomCreate

The bloom filter for NIST 800-63-3B banned password checking.

## Steps to make a password bloom filter (July 2020)

1. Crack as many HIBP SHA1 hashes as possible. Shoot for 90% as a minimum.
   The HIBP v6 release (July 2020) is 572 million hashes. Try to crack at
   least 520 million of them to get good coverage.

2. Obtain as many other individual password dumps as possible. Rockyou, etc.
   These may already be subsets of HIBP, but it won't hurt to have them
   as exposed password files.

3. Now that we have a lot of exposed passwords, eliminate the ones that do
   not meet the org's password policy. At a minimum, eliminate the ones
   that are too short. Also, sort and uniq the hashes.
	```bash
    $ cd words
	$ for f in $(ls *.txt); do wm -policy -words $f | wm -ascii | wm -sha1 >> hashes; done
	$ sort -u -T . hashes -o hashes
	```

4. Get the line count of the hashes and bump cardinality in create.java
   and check.java then build the jars:

	```bash
    wc -l hashes
	vim create.java and check.java (to bump cardinality)
	make && cd check && make
	```

5. Create and test the new filter:

	```bash
    java -jar create.jar hashes vt-banned-passwords.filter.new
    java -jar check.jar vt-banned-passwords.filter.new
	```

6. Commit and push the changes to git, then tag and push the tag:

	```bash
    git commit -am "Updated filter to reflect HIBP v4 changes"
    git push

    git tag -a v2.0 $COMMIT-ID
    git push origin v2.0
	```

7. Test a few words that should be in the filter against the middleware API endpoint:

	```bash
	curl -X POST -H "Content-Type: application/x-www-form-urlencoded" \
	-d "userid=user&type=pid&with=messages&password=Mypassword12345" \
	https://apps.middleware.vt.edu/ws/v1/password/validate | jq .
	```
