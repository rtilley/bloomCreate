PROGRAM = create
SOURCE = $(PROGRAM).java

build:
	javac -cp lib/guava-25.1-jre.jar $(PROGRAM).java
	jar cfm $(PROGRAM).jar Manifest.txt $(PROGRAM).class

clean:
	rm -f $(PROGRAM).class
