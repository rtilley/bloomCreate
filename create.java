// Requires Java 8 or newer

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class create {
  private static final String usage =
      "java -jar create.jar /path/to/sha1/hashes.txt /path/to/output/filter.txt";

  /** Load the hashes into the Bloom filter and save the filter as a file. */
  public static void load_hashes(String fileOfSHA1Hashes, int cardinality, String outputFilter) {

    BloomFilter<byte[]> filter = BloomFilter.create(Funnels.byteArrayFunnel(), cardinality, 0.001);

    try {
      FileInputStream fstream = new FileInputStream(fileOfSHA1Hashes);
      DataInputStream input = new DataInputStream(fstream);
      BufferedReader buffer = new BufferedReader(new InputStreamReader(input));

      String line;

      while ((line = buffer.readLine()) != null) {
        filter.put(line.trim().getBytes());
      }

      input.close();
    } catch (Exception e) {
      System.err.println("load_hashes error: " + e.getMessage());
    }

    // Write the output filter to a file
    try {
      FileOutputStream fos = new FileOutputStream(outputFilter);
      filter.writeTo(fos);
      fos.close();
    } catch (Exception e) {
      System.err.println("Saving output filter error: " + e.getMessage());
    }
  }

  /** Count the hashes in the file and return that count as an int. */
  public static int set_cardinality(String fileOfSHA1Hashes) {
    int lines = 0;
    try {
      BufferedReader reader = new BufferedReader(new FileReader(fileOfSHA1Hashes));
      while (reader.readLine() != null) lines++;
      reader.close();
    } catch (Exception e) {
      System.err.println("Counting hashes error: " + e.getMessage());
    }
    return lines;
  }

  /** main */
  public static void main(String[] args) {
    if (args.length == 2) {
      int count = set_cardinality(args[0]);
      System.err.println("Cardinality: " + count);
      load_hashes(args[0], count, args[1]);
    } else {
      System.out.println(usage);
    }
  }
}
